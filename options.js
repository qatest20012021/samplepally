module.exports = {

    axeOptions : {
        runners: [
            'axe'
        ]
    },

    htmlcsOptions : {
        runners: [
            'htmlcs'
        ]
    }
}