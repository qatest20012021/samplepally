const pa11y = require('pa11y');
const html = require('pa11y-reporter-html');
const report = require('./report');
const options = require('./options');

var url = 'https://gitlab.com/' // Your URL here

pa11y(url, options.htmlcsOptions).then(async results => {    
    const htmlResults = await html.results(results);
    report.save("report",htmlResults)   
    
});