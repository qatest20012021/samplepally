# PallyExample
This is a sample Project that demonstrates Pa11y usage. The project contains examples for both axeRunner and htmlCS runner

# Installation
Clone the Project and run npm install to implement dependancies
Project uses two packages. pa11y and pa11y-reporter-html. 

# Runnin Test
node axeRunner.js will run the test in this file. Similarly run htmlcsRunner.js will run tests in this file

# Report
Report will be generated in the /report folder. This will be a HTML report.
You can Custmise report by changing the report.js file

# Options
Options are passed through the options.js file. This can be customised based on project needs

